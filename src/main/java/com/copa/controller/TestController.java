package com.copa.controller;

import com.copa.domain.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author copa
 * @createDate 2021-07-25 21:02
 * @function 导出pdf
 */
@RestController
public class TestController {

    @RequestMapping(value = "/pdf1")
    public void pdf1(Map<String, Object> parameters,
                     HttpServletResponse response) throws IOException {
        ServletOutputStream os = response.getOutputStream();
        try {
            ArrayList<Student> list = new ArrayList<>();
            for (int i = 1; i <= 40; i++) {
                list.add(new Student(i, "copa" + i, i <= 20 ? "一中" : "二中"));
            }
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            parameters = parameters == null ? new HashMap<>() : parameters;
            parameters.put("summary", "我是summary~");
            ClassPathResource resource = new ClassPathResource("jasper/demo1.jasper");
            response.setContentType("application/pdf");
            InputStream jasperStream = resource.getInputStream();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperStream, parameters, dataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
        } catch (JRException e) {
            e.printStackTrace();
        } finally {
            os.flush();
        }
    }

    @RequestMapping(value = "/pdf2")
    public void pdf2(Map<String, Object> parameters,
                     HttpServletResponse response) throws IOException {
        ServletOutputStream os = response.getOutputStream();
        try {
            // 模拟数据
            ArrayList<Score> list = new ArrayList<>();
            for (int i = 1; i <= 4; i++) {
                Score score = new Score();
                switch (i) {
                    case 1:
                        score.setTime("第一次月考");
                        break;
                    case 2:
                        score.setTime("期中");
                        break;
                    case 3:
                        score.setTime("第二次月考");
                        break;
                    case 4:
                        score.setTime("期末");
                        break;
                }
                score.setChineseScore(new Random().nextInt(100));
                score.setMathScore(new Random().nextInt(100));
                score.setEnglishScore(new Random().nextInt(100));

                list.add(score);
            }
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            parameters = parameters == null ? new HashMap<>() : parameters;
            // 入参
            parameters.put("name", "copa");
            ClassPathResource resource = new ClassPathResource("jasper/demo2.jasper");
            response.setContentType("application/pdf");
            InputStream jasperStream = resource.getInputStream();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperStream, parameters, dataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
        } catch (JRException e) {
            e.printStackTrace();
        } finally {
            os.flush();
        }
    }

    @RequestMapping(value = "/pdf3")
    public void pdf3(Map<String, Object> parameters,
                     HttpServletResponse response) throws IOException {
        ServletOutputStream os = response.getOutputStream();
        try {
            ArrayList<SchoolInfo> list = new ArrayList<>();
            for (int i = 1; i <= 20; i++) {
                SchoolInfo schoolInfo;
                if (i <= 10) {
                    schoolInfo = new SchoolInfo("一中", "小" + i, String.valueOf(new Random().nextInt(2)));
                } else {
                    schoolInfo = new SchoolInfo("二中", "小" + i, String.valueOf(new Random().nextInt(2)));
                }
                list.add(schoolInfo);
            }
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            parameters = parameters == null ? new HashMap<>() : parameters;
            ClassPathResource resource = new ClassPathResource("jasper/demo3.jasper");
            response.setContentType("application/pdf");
            InputStream jasperStream = resource.getInputStream();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperStream, parameters, dataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
        } catch (JRException e) {
            e.printStackTrace();
        } finally {
            os.flush();
        }
    }

    @RequestMapping(value = "/pdf4")
    public void pdf4(Map<String, Object> parameters,
                     HttpServletResponse response) throws IOException {
        ServletOutputStream os = response.getOutputStream();
        try {
            ArrayList<StuInfo> list = new ArrayList<>();
            for (int i = 1; i <= 4; i++) {
                StuInfo stuInfo = new StuInfo();
                ArrayList<StuScore> stuScoreList = new ArrayList<>();
                for (int j = 1; j <= 3; j++) {
                    StuScore stuScore = new StuScore();
                    stuScore.setSubject(j == 1 ? "语文" :
                                        j == 2 ? "数学" : "英语")
                            .setScore(new Random().nextInt(100 + 1));
                    stuScoreList.add(stuScore);
                }
                stuInfo.setName("小" + i)
                        .setScoreList(stuScoreList);
                list.add(stuInfo);
            }
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            parameters = parameters == null ? new HashMap<>() : parameters;
            parameters.put("subPath", new ClassPathResource("jasper/demo4_sub.jasper").getPath());
            ClassPathResource resource = new ClassPathResource("jasper/demo4.jasper");
            response.setContentType("application/pdf");
            InputStream jasperStream = resource.getInputStream();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperStream, parameters, dataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
        } catch (JRException e) {
            e.printStackTrace();
        } finally {
            os.flush();
        }
    }
}
