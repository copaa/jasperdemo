package com.copa.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author copa
 * @createDate 2021-07-26 23:30
 * @function
 */
@Data
@AllArgsConstructor
public class SchoolInfo {

    private String school;
    private String name;
    private String sex;
}
