package com.copa.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author copa
 * @createDate 2021-07-25 21:51
 * @function
 */
@Data
@AllArgsConstructor
public class Student {

    private Integer id;

    private String name;

    private String school;
}
