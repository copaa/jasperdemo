package com.copa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author copa
 * @createDate 2021-07-26 11:13
 * @function
 */
@Data
@NoArgsConstructor
public class Score {

    private Integer chineseScore;

    private Integer mathScore;

    private Integer englishScore;

    private String time;
}
