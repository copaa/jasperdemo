package com.copa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author copa
 * @createDate 2021-07-27 0:19
 * @function
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class StuInfo {

    private String name;

    private List<StuScore> scoreList;
}
