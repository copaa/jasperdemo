package com.copa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author copa
 * @createDate 2021-07-25 21:24
 * @function
 */
@SpringBootApplication
public class JasperApplication {

    public static void main(String[] args) {
        SpringApplication.run(JasperApplication.class, args);
    }
}
